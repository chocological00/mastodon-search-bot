import { Config, StrStrKVStore } from './types'; // eslint-disable-line @typescript-eslint/no-unused-vars
import fs from 'fs';

declare global
{
	namespace NodeJS // eslint-disable-line @typescript-eslint/no-namespace
	{
		interface Global 
		{
			config: Config;
		}
	}

}

export default class Utils
{
	public static generate_user_full_handle = (acct: string): string =>
	{
		return (acct.includes('@') ? acct : acct + `@${global.config.instance_url}`);
	}

	public static strip_content = (content: string, { html = true, mention = true, botmention = true, hashtag = true, space = true, trim = true }, username?: string): string =>
	{
		let bot_name_replacer: RegExp | undefined;
		if(username)
			bot_name_replacer = new RegExp(`((@${username})|(@${Utils.generate_user_full_handle(username).replace('.', '\\.')}))`);

		if(html)
		{
			content = content.replace(/(<br).*?>/g, ' ');
			content = content.replace(/(&amp;)/g, '&');
			content = content.replace(/(&quot;)/g, '"');
			content = content.replace(/(&apos;)/g, '\'');
			content = content.replace(/(&lt;)/g, '<');
			content = content.replace(/(&gt;)/g, '>');
			content = content.replace(/<[^>]*>?/gm, '');
		}
		if(mention)
			content = content.replace(/((^| )@[A-Za-z0-9.\-_]+)/gm, '');
		if(botmention && bot_name_replacer)
			content = content.replace(bot_name_replacer, '');
		if(hashtag) // eslint-disable-next-line no-useless-escape
			content = content.replace(/(#[^\u2000-\u206F\u2E00-\u2E7F\s\\'!"#$%&()*+,\-.\/:;<=>?@\[\]^`{|}~]*)/g, '');
		if(space)
			content = content.replace(/\s\s+/g, ' ');
		if(trim)
			content = content.trim();
		return content;
	}

	public static update_config = (
		{ allow_other_instance_users, bot_admin, blacklist_keywords }: 
		{ allow_other_instance_users?: boolean; bot_admin?: string[]; blacklist_keywords?: string[] }
	): void =>
	{
		if(allow_other_instance_users)
			global.config.allow_other_instance_users = allow_other_instance_users;
		if(bot_admin)
			global.config.bot_admins = bot_admin;
		if(blacklist_keywords)
			global.config.blacklist_keywords = blacklist_keywords;
		fs.writeFileSync(__dirname + '/../data/config.json', JSON.stringify(global.config));
	}

	public static parse_command = (command: string): [StrStrKVStore, string] =>
	{
		const obj: StrStrKVStore = {};
		let str = '';
		const words = command.split(' ');
		for(const word of words)
		{
			if(word.startsWith('!!'))
			{
				const word_split = word.split(':');
				obj[word_split[0].substr(2).toLowerCase()] = word_split[1];
			} 
			else 
			{
				str = str.concat(word + ' ');
			} 
		}
		return [obj, str.trim()];
	}

	public static check_if_admin = (acct: string): boolean =>
	{
		const user_fullhandle = Utils.generate_user_full_handle(acct);
		return global.config.bot_admins.includes(user_fullhandle);
	}

	public static sleep = (ms: number): Promise<void> =>
	{
		return new Promise((resolve): void =>
		{
			setTimeout(resolve, ms);
		});
	}
}