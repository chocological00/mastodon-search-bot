import Mastodon, { Relationship, Response, Status, Account, Results } from 'megalodon';
import request from 'request-promise-native';
import Utils from './utils';

export default class Mstdn
{
	private M: Mastodon

	constructor(M: Mastodon)
	{
		this.M = M;
	}

	public follow_user = (account_id: string): Promise<Response<Relationship>> =>
	{
		return this.M.post<Relationship>(`/v1/accounts/${account_id}/follow`);
	}

	public block_unblock_user = async (user_id: string): Promise<void> =>
	{
		await this.M.post<Relationship>(`/v1/accounts/${user_id}/block`);
		await this.M.post<Relationship>(`/v1/accounts/${user_id}/unblock`);
		return;
	}

	public block_user = (user_id: string): Promise<Response<Relationship>> =>
	{
		return this.M.post<Relationship>(`/v1/accounts/${user_id}/block`);
	}

	public unblock_user = (user_id: string): Promise<Response<Relationship>> =>
	{
		return this.M.post<Relationship>(`/v1/accounts/${user_id}/unblock`);
	}

	public get_user_id = async (acct_fullhandle: string): Promise<string | null> =>
	{
		const response = await this.M.get<Results>('/v2/search',
			{
				q: acct_fullhandle,
				resolve: undefined
			});
		
		if(response.data.accounts.length > 0)
			for(const accounts of response.data.accounts)
				if(Utils.generate_user_full_handle(accounts.acct).toLowerCase() === acct_fullhandle.toLowerCase())
					return response.data.accounts[0].id;

		return null;
	}

	public send_reply = (acct: string, content: string, in_reply_to_id: string): Promise<Response<Status>> =>
	{
		return this.M.post<Status>('/v1/statuses', 
			{
				status: `@${acct} ${content}`,
				in_reply_to_id: in_reply_to_id,
				visibility: 'direct'
			});
	}

	public send_dm = (acct: string, content: string): Promise<Response<Status>> =>
	{
		return this.M.post<Status>('/v1/statuses',
			{
				status: `@${acct} ${content}`,
				visibility: 'direct'
			});
	}

	public is_bot = async (user_id: string): Promise<boolean> =>
	{
		const response = await this.M.get<Account>(`/v1/accounts/${user_id}`);
		return Boolean(response.data.bot); // nullable
	}

	public check_if_status_exists = async (url: string): Promise<boolean> =>
	{
		// 이게 URL이 다른 인스턴스 URL일수도 있어서 이렇게 해놓음
		try
		{
			await request(
				{
					uri: url.replace(/(@[A-Za-z0-9.\-_]+)/g, 'api/v1/statuses'),
					json: true
				});
		}
		catch(err) // on 404
		{
			if(err.statusCode === 404)
				return false;
			else return true; // 다른 오류일 경우 검색결과를 삭제할 이유가 없음
		}
		return true;
	}

	public get_current_account = (): Promise<Response<Account>> =>
	{
		return this.M.get<Account>('/v1/accounts/verify_credentials');
	}

	public get_following_accounts_ids = async (account_id: string): Promise<string[]> =>
	{
		return (await this.M.get<Account[]>(`/v1/accounts/${account_id}/following`)).data.map((account) => account.id);
	}

	public favorite_toot = (toot_id: string): Promise<Response<Status>> =>
	{
		return this.M.post<Status>(`/v1/statuses/${toot_id}/favourite`);
	}

	public delete_toot = (toot_id: string): Promise<Response<{}>> =>
	{
		return this.M.del<{}>(`/v1/statuses/${toot_id}`);
	}

	public get_status = (toot_id: string): Promise<Response<Status>> =>
	{
		return this.M.get<Status>(`/v1/statuses/${toot_id}`);
	}
}