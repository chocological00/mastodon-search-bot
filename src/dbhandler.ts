import { Database, Statement } from 'better-sqlite3';
import { oneLineTrim } from 'common-tags';

import { DBRow } from './types';

export default class DatabaseHandler
{
	private db: Database;

	private db_insert: Statement;
	private db_forget_user: Statement;
	private db_search: Statement;
	private db_search_user: Statement;
	private db_search_hash: Statement;
	private db_search_user_hash: Statement;
	private db_delete_url: Statement;

	constructor(db: Database)
	{
		this.db = db;
		this.db.prepare(oneLineTrim`
			CREATE TABLE IF NOT EXISTS search_database
			(id INTEGER PRIMARY KEY AUTOINCREMENT, 
			user TEXT NOT NULL, 
			url TEXT NOT NULL,
			content TEXT NOT NULL, 
			unixtimeutc INTEGER NOT NULL,
			cw TEXT DEFAULT NULL, 
			hashtags TEXT DEFAULT NULL,
			attachment INTEGER NOT NULL DEFAULT 0)`).run();
		
		this.db_insert =this.db.prepare(oneLineTrim`
			INSERT INTO search_database(user, url, content, unixtimeutc, cw, hashtags, attachment) 
			VALUES (?, ?, ?, ?, ?, ?, ?)`);
		this.db_forget_user = this.db.prepare(oneLineTrim`
			DELETE FROM search_database 
			WHERE user = ?`);
		this.db_search = this.db.prepare(oneLineTrim`
			SELECT * FROM search_database 
			WHERE content LIKE ? COLLATE NOCASE ORDER BY unixtimeutc DESC`);
		this.db_search_user = this.db.prepare(oneLineTrim`
			SELECT * FROM search_database 
			WHERE (content LIKE ? COLLATE NOCASE AND user = ?) ORDER BY unixtimeutc DESC`);
		this.db_search_hash = this.db.prepare(oneLineTrim`
			SELECT * FROM search_database 
			WHERE (content LIKE ? COLLATE NOCASE AND hashtags LIKE ?) ORDER BY unixtimeutc DESC`);
		this.db_search_user_hash = this.db.prepare(oneLineTrim`
			SELECT * FROM search_database 
			WHERE (content LIKE ? COLLATE NOCASE AND user = ? AND hashtags LIKE ?) ORDER BY unixtimeutc DESC`);
		this.db_delete_url = this.db.prepare(oneLineTrim`
			DELETE FROM search_database WHERE url = ?`);
	}

	public insert = (user_fullhandle: string, url: string, content: string, unixtimeutc: number, cw?: string, hashtags?: string, attachment?: boolean): void =>
	{
		const basicvalues = [user_fullhandle.toLowerCase(), url.toLowerCase(), content, unixtimeutc];
		const cw_or_null = cw ? cw : null;
		const hashtags_or_null = hashtags ? hashtags.toLowerCase() : null;
		const one_or_zero = attachment ? 1 : 0;

		this.db_insert.run(basicvalues, cw_or_null, hashtags_or_null, one_or_zero);
	}

	public search = (searchstring: string, 
		{user_fullhandle, hashtags, attachment }: 
		{ user_fullhandle?: string; hashtags?: string; attachment?: boolean }
	): DBRow[] =>
	{
		let rows: DBRow[];

		if(user_fullhandle&&hashtags)
			rows = this.db_search_user_hash.all(searchstring, user_fullhandle.toLowerCase(), hashtags.toLowerCase());
		else if(user_fullhandle)
			rows = this.db_search_user.all(searchstring, user_fullhandle.toLowerCase());
		else if(hashtags)
			rows = this.db_search_hash.all(searchstring, hashtags.toLowerCase());
		else
			rows = this.db_search.all(searchstring);

		return attachment ? rows.filter((row) => Boolean(row.attachment)) : rows;
	}

	public forget_user = (user_fullhandle: string): void =>
	{
		this.db_forget_user.run(user_fullhandle.toLowerCase());
	}

	public delete_url = (url: string): void =>
	{
		this.db_delete_url.run(url.toLowerCase());
	}
}