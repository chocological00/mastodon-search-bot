// TODO: 페더레이션 기능 추가? 어쩌면

import fs from 'fs';
import url from 'url';
import winston from 'winston';
import Mastodon, { Notification, StreamListener } from 'megalodon';
import sqlite3 from 'better-sqlite3';
import { oneLineTrim } from 'common-tags';

import StreamHandler from './streamhandler';

import { Config, Credentials, BackupJSON } from './types'; // eslint-disable-line @typescript-eslint/no-unused-vars
import Utils from './utils';

declare global
{
	namespace NodeJS // eslint-disable-line @typescript-eslint/no-namespace
	{
		interface Global 
		{
			logger: winston.Logger;
			config: Config;
		}
	}

}

const dirname = __dirname + '/../..';

const main = async (): Promise<void> =>
{
	// Winston 셋업
	global.logger = winston.createLogger(
		{
			level: 'info',
			format: winston.format.simple(),
			transports: 
			[
				new winston.transports.File({ filename: dirname + '/data/logs/error.log', level: 'warn' }),
				new winston.transports.File({ filename: dirname + '/data/logs/combined.log', level: 'info'}),
				// new winston.transports.File({ filename: __dirname + '/data/logs/combined_debug.log', level: 'debug'}),
				new winston.transports.Console({ format: winston.format.simple(), level: 'debug' })
			]
		});

	// 설정파일 읽기
	global.config = JSON.parse(fs.readFileSync(dirname + '/data/config.json', 'utf8'));
	const credentials: Credentials = JSON.parse(fs.readFileSync(dirname + '/data/credentials.json', 'utf8'));
	global.logger.info('Config Read!');
	
	// DB 준비
	const db = sqlite3(dirname + '/data/database.sqlite3', { timeout: 60*1000 });
	global.logger.info('DB preparation successful!');
	
	// 맛돈 API 준비
	const M = new Mastodon(credentials.access_token, 'https://' + global.config.instance_url + '/api');
	global.logger.info('API Preparation Successful!');

	// StreamHandler 준비
	const stream = new StreamHandler(M, db);
	global.logger.info('Initializing StreamHandler...');
	await stream.Ready;
	global.logger.info('StreamHandler Initialization Successful!');

	// helper functions
	const sleep = (milisec: number): Promise<void> =>
	{
		return new Promise((resolve): void =>
		{
			setTimeout(resolve, milisec);
		});
	};

	// 백업 json watch
	const file_watcher = fs.watch(dirname + '/watch');
	file_watcher.on('change', async (event, filename) => // eslint-disable-line @typescript-eslint/no-misused-promises
	{
		if(event !== 'rename') return;
		if(process.env.NODE_ENV !== 'dev')
			await Utils.sleep(120 * 1000); // SSH 파일 전송 속도 감안해서 딜레이 좀 두기
		if(!fs.existsSync(dirname + '/watch/' + filename)) return;
		global.logger.info('New file added in watch');
		const imported_json: BackupJSON = JSON.parse(fs.readFileSync(dirname + '/watch/' + filename, 'utf8'));

		let last_toot_unixtime: number | undefined;
		let last_toot_user_handle: string | undefined;

		let index_reverse = 0;
		do
		{
			const toot = imported_json.orderedItems[imported_json.orderedItems.length - ++index_reverse];
			if(typeof toot.object === 'string') continue;
			last_toot_unixtime = Math.round((new Date(toot.object.published)).getTime() / 1000);
			const hostname = url.parse(toot.object.url).hostname;
			const username = (/(@[A-Za-z0-9.\-_]+)/g).exec(toot.object.url);
			if(username)
				last_toot_user_handle = username[0].replace('@', '') + '@' + hostname;
		} while(typeof last_toot_unixtime === 'undefined' || typeof last_toot_user_handle === 'undefined');

		db.prepare('DELETE FROM search_database WHERE unixtimeutc <= ? AND user = ?').run(last_toot_unixtime, last_toot_user_handle);

		for(const [index, item] of imported_json.orderedItems.entries())
		{
			if(index%(Math.floor(imported_json.orderedItems.length/100))===0)
				global.logger.info(`Processing ${filename}: ${Math.round(index/imported_json.orderedItems.length*100)}%`);
			if(index === imported_json.orderedItems.length - 1)
				global.logger.info(`${filename} process finishied!`);
			// item.cc 비어있으면 private 또는 DM
			if(item.cc.length === 0) continue;
			// BT는 item.object가 그냥 url임
			if(typeof item.object === 'string') continue;

			// 봇에게 보내는 명령어는 크롤링 제외
			if(item.to.includes('https://planet.moe/users/search')) continue;
			// TODO: @search 하드코딩 자제졈..

			const hostname = url.parse(item.object.url).hostname;
			const username = (/(@[A-Za-z0-9.\-_]+)/g).exec(item.object.url);
			let user_fullhandle: string | undefined;
			if(username) // 플래닛 아닐수도 있으니까 Util에 있는거 말고 여기서 처리
				user_fullhandle = username[0].replace('@', '') + '@' + hostname;
			const toot_address: string = item.object.url;
			const stripped_content = Utils.strip_content(item.object.content, {});
			const unixtimeutc = Math.round((new Date(item.object.published)).getTime() / 1000);
			const cw: string | null = item.object.summary ? item.object.summary : null; // nullable이라서 이렇게함
			const tags = [];
			if(item.object.tag && item.object.tag.length !== 0)
				for(const tag of item.object.tag)
					if(tag.type === 'Hashtag')
						tags.push(tag.name.replace('#', ''));
			const hashtags: string | null = tags.length !== 0 ?
				tags.sort().reduce((acc, cur) => acc + cur + ' ', '').trim():
				null;
			const attachment = item.object.attachment.length !== 0 ? 1 : 0;

			if(user_fullhandle === undefined || toot_address === undefined || stripped_content === undefined || unixtimeutc === undefined)
				continue;

			// FIXME: DB 오브젝트 여기로 끌어오려면 귀찮아지니까 여기서 바로 처리..했는데 좋은 방법은 아닌것같다..
			db.prepare(oneLineTrim`
				INSERT INTO search_database(user, url, content, unixtimeutc, cw, hashtags, attachment) 
				VALUES (?, ?, ?, ?, ?, ?, ?)`).run(user_fullhandle, toot_address, stripped_content, unixtimeutc, cw, hashtags, attachment);
		}

		fs.unlinkSync(dirname + '/watch/' + filename);
	});

	const listener: StreamListener = M.stream('/v1/streaming/user');
	global.logger.info('Stream created!');

	listener.on('update', stream.on_home_update);

	listener.on('notification', (noti: Notification): void =>
	{
		switch(noti.type)
		{
		case 'mention':
			stream.on_mention(noti); break;
		case 'follow':
			stream.on_follow(noti); break;
		case 'favourite': // 영국놈들 스펠링 이상해ㅡㅡ
			stream.on_favorite(noti); break;
		}
	});
};

main();