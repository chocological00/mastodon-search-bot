export interface StrStrKVStore
{
	[key: string]: string;
}

export interface Config
{
	instance_url: string;
	allow_other_instance_users: boolean;
	bot_admins: string[];
	blacklist_keywords: string[];
}

export interface Credentials
{
	access_token: string;
}

export interface DBRow
{
	id: number;
	user: string;
	url: string;
	content: string;
	unixtimeutc: number;
	cw: string | null;
	hashtags: string | null;
	attachment: number;
}

export interface BackupJSON
{
	'@context': string;
	id: string;
	type: string;
	totalItems: number;
	orderedItems:
		[
			{
				'@context':
					[
						string,
						{
							ostatus: string;
							atomUri: string;
							inReplyToAtomUri: string;
							conversation: string;
							sensitive: string;
							Hashtag: string;
							toot: string;
							Emoji: string;
							focalPoint:
								{
									'@container': string;
									'@id': string;
								};
							blurhash: string;
						}
					];
				id: string;
				type: string;
				actor: string;
				published: string;
				to: string[];
				cc: string[];
				object: 
					{
						id: string;
						type: string;
						summary: string | null;
						inReplyTo: string | null;
						published: string;
						url: string;
						attributedTo: string;
						to: string[];
						cc: string[];
						sensitive: boolean;
						atomUri: string;
						inReplyToAtomUri: string | null;
						conversation: string;
						content: string;
						contentMap: StrStrKVStore;
						attachment: string[];
						tag:
							{
								type: string;
								href: string;
								name: string;
							}[];
						replies: 
							{
								id: string;
								type: string;
								first:
									{
										type: string;
										partOf: string;
										items: string[];
									};
							};
					} | string;
			}
		];
}