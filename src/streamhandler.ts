import { Status,  Notification, Account } from 'megalodon';
import { stripIndent } from 'common-tags';
import { Logger } from 'winston'; // eslint-disable-line @typescript-eslint/no-unused-vars
import Mastodon from 'megalodon';
import { Database } from 'better-sqlite3';

import Mstdn from './mstdn';
import DatabaseHandler from './dbhandler';
import Utils from './utils';

import { Config, StrStrKVStore } from './types'; // eslint-disable-line @typescript-eslint/no-unused-vars

declare global
{
	namespace NodeJS // eslint-disable-line @typescript-eslint/no-namespace
	{
		interface Global 
		{
			logger: Logger;
			config: Config;
		}
	}

}

export default class StreamHandler
{
	public Ready: Promise<void>;

	private eula_pending: StrStrKVStore
	private account: Account | null
	private currently_following_ids: string[]
	private mstdn: Mstdn
	private db: DatabaseHandler

	constructor(M: Mastodon, db: Database)
	{
		// 일반적인 것들
		this.mstdn = new Mstdn(M);
		this.db = new DatabaseHandler(db);
		this.eula_pending = {};

		// 안 말짱한 것들 ㅡㅡ
		this.account = null;
		this.currently_following_ids = [];
		this.Ready = this.init_async_stuff();
	}

	private init_async_stuff = async(): Promise<void> =>
	{
		this.account = (await this.mstdn.get_current_account()).data;
		this.currently_following_ids = await this.mstdn.get_following_accounts_ids(this.account.id);

		return;
	}

	public on_home_update = (status: Status): void =>
	{
		// TS 타입체크
		if(!this.account) return;

		// 무시할것들
		if(status.account.id === this.account.id) return; // (내가) 봇이 쓴 툿
		if(status.reblog !== null) return; // 유감! 부스트였습니다!
		if(status.visibility !== 'unlisted' && status.visibility !== 'public') return; // public / unlisted 아니면 스킵
		if(((): boolean => // 봇에게 보내는 명령어는 크롤링 제외
		{
			for(const mention of status.mentions) 
				if (mention.id === this.account.id) return true;
			return false;
		})()) return;
		// TESTME: API DOCs 에는 null값 있을수 있다고 되있는데 어떤 케이스인지를 몰겠음..
		if(!status.url) // url 비었을경우
		{
			global.logger.warn('toot url is null!');
			global.logger.warn(status);
			return;
		}

		// DB에 들어갈것들
		const user_fullhandle = Utils.generate_user_full_handle(status.account.acct);
		const url = status.url;
		const stripped_content = Utils.strip_content(status.content, {}, this.account.username);
		const unixtimeutc = Math.round((new Date(status.created_at)).getTime() / 1000);
		const cw = status.spoiler_text ? status.spoiler_text : undefined;
		const hashtags = status.tags.length !== 0 ?
			status.tags.sort((a, b) => (a.name > b.name) ? 1 : -1).reduce((acc, cur) => acc + cur.name + ' ', '').trim():
			undefined;
		const attachment = status.media_attachments.length !== 0 ? true : false;

		// DB에 날리기
		this.db.insert(user_fullhandle, url, stripped_content, unixtimeutc, cw, hashtags, attachment);
	}
	
	public on_mention = async (noti: Notification): Promise<void> =>
	{
		// TS 타입체크
		if(!this.account) return;

		// 무시할것들
		if(noti.status === null) return; // 아마 그럴일은 없겠지만 TS가 불평하므로 체크
		if(await this.mstdn.is_bot(noti.account.id)) return; // 봇한테서 온 멘션 무시

		// 유저가 블언블후에 !!forgetme날릴수도 있으니 이거 먼저 처리
		if(noti.status.content.includes('!!forgetme'))
		{
			await this.mstdn.block_unblock_user(noti.account.id);
			const user_fullhandle = Utils.generate_user_full_handle(noti.account.acct);
			this.db.forget_user(user_fullhandle);
			this.currently_following_ids = this.currently_following_ids.filter((elem) => elem !== noti.account.id);
			await this.mstdn.send_reply(noti.account.acct, 
				'유저분의 모든 툿을 DB에서 삭제했어요!',
				noti.status.id);
			return;
		}

		// 봇이 팔로우하지 않은 사람에게서 메시지가 왔다!
		if(!this.currently_following_ids.includes(noti.account.id))
		{
			await this.mstdn.send_reply(noti.account.acct,
				'죄송하지만 봇의 기능은 봇을 팔로우하신 후 안내 조항에 동의하신 후에만 사용하실 수 있어요!',
				noti.status.id);
			return;
		}

		const stripped_content = Utils.strip_content(noti.status.content, {}, this.account.username);
		const [parsed_command, stripped_stripped_content] = Utils.parse_command(stripped_content);

		// 명령어 처리

		// 관리자 명령어
		if(Utils.check_if_admin(noti.account.acct))
		{
			// 밴
			if(Object.prototype.hasOwnProperty.call(parsed_command, 'ban'))
			{
				const ban_user_fullhandle = parsed_command['ban'];
				if(!ban_user_fullhandle || ban_user_fullhandle.split('@').length !== 2) // from: 다음에 공백이거나, @가 2+개 있거나, 아예 없을경우
				{
					await this.mstdn.send_reply(noti.account.acct,
						stripIndent`
						'!!ban:'과 '유저네임' 사이에 공백이 있으면 안돼요!
						또, 유저네임을 입력할 때는:
						1) 인스턴스명을 꼭 붙여주시고,
						2) 맨 앞의 @는 빼주세요!
						ex) '!!ban: @ search' 가 아닌 '!!ban:search@planet.moe`,
						noti.status.id);
					return;
				}

				this.db.forget_user(ban_user_fullhandle); // 스팸계가 삭제했을수도 있으니 이거 먼저

				const ban_user_id = await this.mstdn.get_user_id(ban_user_fullhandle);
				if(ban_user_id === null)
				{
					await this.mstdn.send_reply(noti.account.acct,
						`DB에 삭제 쿼리는 날렸지만, 해당 유저 ${ban_user_fullhandle}가 존재하지 않는 것 같습니다!`,
						noti.status.id);
					return;
				}

				await this.mstdn.block_user(ban_user_id);
				this.currently_following_ids = this.currently_following_ids.filter((elem) => elem !== ban_user_id);
				await this.mstdn.send_reply(noti.account.acct, 
					`계정 ${ban_user_fullhandle} (Instance ID: ${ban_user_id}) 차단 및 DB에서 삭제 완료!`,
					noti.status.id);
				return;
			}

			// 언밴
			if(Object.prototype.hasOwnProperty.call(parsed_command, 'unban'))
			{
				const unban_user_fullhandle = parsed_command['unban'];
				if(!unban_user_fullhandle || unban_user_fullhandle.split('@').length !== 2) // from: 다음에 공백이거나, @가 2+개 있거나, 아예 없을경우
				{
					await this.mstdn.send_reply(noti.account.acct,
						stripIndent`
						'!!unban:'과 '유저네임' 사이에 공백이 있으면 안돼요!
						또, 유저네임을 입력할 때는:
						1) 인스턴스명을 꼭 붙여주시고,
						2) 맨 앞의 @는 빼주세요!
						ex) '!!unban: @ search' 가 아닌 '!!unban:search@planet.moe`,
						noti.status.id);
					return;
				}

				const unban_user_id = await this.mstdn.get_user_id(unban_user_fullhandle);
				if(unban_user_id === null)
				{
					await this.mstdn.send_reply(noti.account.acct,
						`해당 유저 ${unban_user_fullhandle}가 존재하지 않는 것 같습니다!`,
						noti.status.id);
					return;
				}

				await this.mstdn.unblock_user(unban_user_id);
				await this.mstdn.send_reply(noti.account.acct,
					`계정 ${unban_user_fullhandle} (Instance ID: ${unban_user_id}) 차단 해제 완료!`,
					noti.status.id);
				return;
			}
		}

		// 도움말
		if(Object.prototype.hasOwnProperty.call(parsed_command, 'help'))
		{
			const param = parsed_command['help'];
			let reply = '';
			switch(param)
			{
			case 'help':
				reply = 
					stripIndent`
					검색어와 함께 사용 불가
					도움말을 불러옵니다. 명령어와 함께 사용할 경우 명령어 도움말을 불러옵니다.
					예) !!help 또는 !!help:help`;
				break;

			case 'forgetme':
				reply = 
					stripIndent`
					검색어와 함께 사용 불가
					검색봇에 인덱스된 유저분의 모든 툿을 삭제하고 더 이상 새로운 툿을 인덱싱하지 않습니다.
					예) !!forgetme`;
				break;

			case 'from':
				reply = 
					stripIndent`
					검색어와 함께 사용 가능
					!!from:<유저네임>
					'유저네임' 유저가 작성한 툿만 검색합니다. 해당 유저도 검색봇을 사용중인 경우에만 검색이 가능합니다.
					유저네임은 반드시 search@instance.name 의 포맷이여야 합니다 (맨 앞의 @는 빼주세요!).
					예) !!from:search@planet.moe 검색어`;
				break;

			case 'attachment':
				reply = 
					stripIndent`
					검색어와 함께 사용 가능
					사진 또는 첨부파일이 포함된 툿만 검색합니다.
					예) !!attachment 검색어
					`;
				break;

			default:
				reply = 
					stripIndent`
					봇 사용법:
					<명령어> <검색어 (지원되는 명령어의 경우)> 혹은 <검색어>
					* 검색어는 영문만 있을 경우 5글자 이상, 다른 경우 3글자 이상이여야 합니다. (앞뒤 공백 제외)
					* 자연어 처리 그런거 없어요.. 토씨 하나까지 맞는 툿만 검색됨!

					사용 가능한 명령어들을 보시려면 !!commands를 사용하세요!`;
				break;
			}

			await this.mstdn.send_reply(noti.account.acct, reply, noti.status.id);
			return;
		}

		if(Object.prototype.hasOwnProperty.call(parsed_command, 'commands'))
		{
			await this.mstdn.send_reply(noti.account.acct,
				stripIndent`
					사용 가능한 명령어 목록:
					!!help - 도움말을 불러옵니다. (검색어와 함께 사용 불가)
					!!forgetme - 봇의 인덱싱 동의를 철회합니다. 봇에 인덱싱된 유저분의 모든 툿이 삭제됩니다. (검색어와 함께 사용 불가)
					!!from:<search@planet.moe> <검색어> - search@planet.moe 계정이 보낸 툿에서만 검색합니다. (맨 앞에 @는 빼주세요!)
					!!attachment - 사진 / 첨부파일이 있는 툿만 검색합니다.
					
					더 자세한 내용을 보시려면 !!help:<명령어>를 사용하세요!`,
				noti.status.id);
			return;
		}

		// 일반 검색		
		let username_fullhandle: string | undefined;
		let tags: string | undefined;
		let attachment: boolean | undefined;

		// 명령어 처리

		// attachment
		if(Object.prototype.hasOwnProperty.call(parsed_command, 'attachment'))
			attachment = true;

		// from
		if(Object.prototype.hasOwnProperty.call(parsed_command, 'from'))
		{
			username_fullhandle = parsed_command['from'];
			if(!username_fullhandle || username_fullhandle.split('@').length !== 2) // from: 다음에 공백이거나, @가 2+개 있거나, 아예 없을경우
			{
				await this.mstdn.send_reply(noti.account.acct,
					stripIndent`
					'!!from:'과 '유저네임' 사이에 공백이 있으면 안돼요!
					또, 유저네임을 입력할 때는:
					1) 인스턴스명을 꼭 붙여주시고,
					2) 맨 앞의 @는 빼주세요!
					ex) '!!from: @ search' 가 아닌 '!!from:search@planet.moe`,
					noti.status.id);
				return;
			}
		}
		if(noti.status.tags.length !== 0) // 해시태그 있는 검색
			tags = noti.status.tags.sort((a, b) => (a.name > b.name) ? 1 : -1).reduce((acc, cur) => acc + cur.name + '%', '%').trim();

		// 블랙리스트 / 글자수 처리
		let valid = true;
		let blacklist = false;
		const searchstring = stripped_stripped_content.split(' ').reduce((acc, cur) => 
		{
			if(!valid) return;
			if(global.config.blacklist_keywords.includes(cur)) { valid = false; blacklist = true; return; }
			const ex_ascii_only = /^[\x00-\xFF]*$/.test(cur); // eslint-disable-line no-control-regex
			if(ex_ascii_only) { if(cur.length < 5) { valid = false; return; } }
			else { if(cur.length < 3) { valid = false; return; } }
			return acc + cur + '%';
		}, '%');
		if(!valid || !searchstring) // 문제 있을 경우
		{
			await this.mstdn.send_reply(noti.account.acct,
				blacklist ? 
					'블랙리스트에 올라간 키워드가 포함되어 있어요!' : 
					'영어는 5글자 이상, 그 외에는 3글자 이상이여야 해요!',
				noti.status.id);
			return;
		}

		// DB 쿼리
		const startTime1 = Math.round(new Date().getTime() / 10) / 100;
		const results = this.db.search(searchstring, { user_fullhandle: username_fullhandle, hashtags: tags, attachment: attachment });
		if(results.length === 0) // 결과 없을 경우
		{
			await this.mstdn.send_reply(noti.account.acct, 
				'검색결과가 없어요! :<', noti.status.id);
			return;
		}
		const elapsedTime1 = (Math.round(new Date().getTime() / 10) / 100) - startTime1;

		await this.mstdn.favorite_toot(noti.status.id);

		const startTime2 = Math.round(new Date().getTime() / 10) / 100;
		const new_results = [];
		for(const result of results) // NOTE: map.filter는 async 지원 안함 / map 사용해서 한다해도 리퀘 async로 날리면 서버에서 리밋날릴수도 있음
		{
			if(!(await this.mstdn.check_if_status_exists(result.url))) // 만약 툿이 지워진 경우
				this.db.delete_url(result.url); // 조용히 DB에서 삭제
			else new_results.push(result);
		}
		const elapsedTime2 = (Math.round(new Date().getTime() / 10) / 100) - startTime2;

		// 유저한테 답장
		let prev_reply_id = noti.status.id; // 검색툿 ID
		const head_response = await this.mstdn.send_reply(noti.account.acct, 
			`총 ${new_results.length}건의 결과 (DB 쿼리 ${elapsedTime1.toFixed(2)}초, 툿 삭제 체크 ${elapsedTime2.toFixed(2)}초)`,
			prev_reply_id); // eslint-disable-line indent
		prev_reply_id = head_response.data.id; // 결과 요약 툿 ID
		for(const result of new_results)
		{
			const date = new Date(result.unixtimeutc * 1000);
			const preview = result.cw ? `CW: ${result.cw}` : (result.content.length > 100 ? result.content.substring(0, 100).trim() + '[...]' : result.content);
			const message = stripIndent`
				${result.user}가 ${date.getUTCFullYear()}/${date.getUTCMonth()+1}/${date.getUTCDate()}에 작성:
				${preview}
				링크: ${result.url}`;
			const response = await this.mstdn.send_reply(noti.account.acct, message, prev_reply_id);
			prev_reply_id = response.data.id;
		}
	}

	public on_follow = async (noti: Notification): Promise<void> =>
	{
		// 이미 봇이 팔로우중인 유저일경우 (언팔당했다 다시 팔로잉됐을때)
		if(this.currently_following_ids.includes(noti.account.id)) return;

		// 플래닛 아닌유저가 팔로했을경우
		if((!global.config.allow_other_instance_users) && noti.account.acct.includes('@'))
		{
			// 블언블 ( NOTE: 팔로워 삭제 기능 API Docs에 없음 웹에서도 블언블로 하는건가?? )
			await this.mstdn.block_unblock_user(noti.account.id);
			await this.mstdn.send_dm(noti.account.acct,
				`현재 검색봇은 ${global.config.instance_url}의 유저들에게만 열려 있어요! :<`);
			return;
		}

		// 플래닛 맞음
		const response = await this.mstdn.send_dm(noti.account.acct,
			stripIndent`
				검색봇을 팔로우해주셔서 감사합니다! '!!help'로 명령어 도움말을 보실 수 있습니다!
				이 봇을 팔로우함으로써 여러분의 공개 / 타임라인에 비표시 툿이 검색을 위해 봇의 서버에 인덱싱된다는 점을 기억해주세요!
				위 사항에 동의하신다면 이 툿에 별을 찍어주세요! (동의하기 전까지 검색 기능을 사용하실 수 없습니다.)
				동의를 철회하고 싶으시다면 봇에게 DM으로 !!forgetme 라고 보냄으로써 봇에 인덱싱된 유저분의 모든 툿을 삭제할 수 있습니다!
				별을 찍었다 해제하는 것 / 언팔 / 단순 블락으로 동의가 철회되지 않는다는 점에 주의하세요!!
				P.S. 잠겨 있는 계정의 경우 이 툿에 별을 찍은 후 팔로우 요청 목록에서 검색봇의 팔로우를 승인해주셔야 합니다.`);
		this.eula_pending[response.data.id] = noti.account.id; // pending kvstore에 동의 안내 툿 id - 계정 id

		return;
	}

	public on_favorite = async (noti: Notification): Promise<void> =>
	{
		// 무시할 것들
		if(noti.status === null) return;
		if(!(noti.status.id in this.eula_pending)) // 별찍힌게 EULA 동의 안내가 아니면 쓰레드의 봇 툿을 순서대로 전부 삭제
		{
			let to_delete = noti.status; // 지울 툿 객체
			while(true) // eslint-disable-line no-constant-condition
			{
				if(this.account === null) continue; // TS 타입체크
				if(to_delete.account.id === this.account.id) // 봇이 쓴 툿이면
				{
					await this.mstdn.delete_toot(to_delete.id); // 삭제
					if(to_delete.in_reply_to_id === null) break; // 쓰레드 끝이면 break
					to_delete = (await this.mstdn.get_status(to_delete.in_reply_to_id)).data; // 새로 지울 툿 받아오기
				} 
				else break; // 봇이 쓴 툿 아니면 break
			}
			return;
		}
		if(noti.account.id !== this.eula_pending[noti.status.id]) return; // 별찍은사람이 EULA에 연동된 사람이 아니라면 역시 무시 (DM이라 체크 필요없을것 같긴 한데)

		// 해당 유저 팔로우
		await this.mstdn.follow_user(noti.account.id);
		delete this.eula_pending[noti.status.id]; // 펜딩에서 삭제
		this.currently_following_ids.push(noti.account.id); // 팔로잉에 추가

		return;
	}
}
