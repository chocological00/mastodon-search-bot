import sqlite3 from 'better-sqlite3';

const olddb = sqlite3(__dirname + '/olddb.sqlite3');
const newdb = sqlite3(__dirname + '/newdb.sqlite3');

newdb.prepare('CREATE TABLE IF NOT EXISTS search_database(id INTEGER PRIMARY KEY AUTOINCREMENT, user TEXT NOT NULL, url TEXT NOT NULL,content TEXT NOT NULL, unixtimeutc INTEGER NOT NULL, cw TEXT DEFAULT NULL, hashtags TEXT DEFAULT NULL, attachment INTEGER NOT NULL DEFAULT 0)').run();

const newdb_insert = newdb.prepare('INSERT INTO search_database(user, url, content, unixtimeutc, cw, hashtags, attachment) VALUES (?, ?, ?, ?, ?, ?, ?)');

const old_records = olddb.prepare('SELECT * FROM search_database').all();

const total = old_records.length;

// eslint-disable-next-line @typescript-eslint/no-for-in-array
for(const index in old_records)
{
	console.log(`${parseInt(index)+1}/${total}`);
	const old_record = old_records[index];
	const unixtimeutc = new Date(old_record.utcstring).getTime() / 1000;
	const content = old_record.content.replace(/(&amp;)/g, '&').replace(/(&quot;)/g, '"').replace(/(&apos;)/g, '\'').replace(/(&lt;)/g, '<').replace(/(&gt;)/g, '>');
	newdb_insert.run(old_record.user, old_record.url, content, unixtimeutc, old_record.cw, old_record.hashtags, 0);
}